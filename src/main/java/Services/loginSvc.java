package Services;

import jakarta.servlet.http.HttpServletRequest;

import java.util.Optional;

public interface loginSvc {
    Optional<String> getUsuario(HttpServletRequest req);
}
