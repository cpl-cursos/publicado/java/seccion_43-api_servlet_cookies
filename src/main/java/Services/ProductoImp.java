package Services;

import modelos.Producto;

import java.util.Arrays;
import java.util.List;

public class ProductoImp implements ProductoSvc {
    @Override
    public List<Producto> listar() {
        return Arrays.asList(
                new Producto(1L, "Portatil", "pc", 1200L),
                new Producto(2L, "Sobremesa", "pc", 1800L),
                new Producto(3L, "teclado", "accesorios", (long) 35.26)
        );
    }

    public String buscar() {
        return "Encontrado";
    }
}
