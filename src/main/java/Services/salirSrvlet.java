package Services;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@WebServlet("/salir")
public class salirSrvlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        loginSvc aut = new loginSvcImpl();
        Optional<String> ckopt = aut.getUsuario(req);

        // ... si existe, el usuario es válido ...
        if (ckopt.isPresent()) {
            // ... creamos una cookie vacia con el mismo nombre
            Cookie ck = new Cookie("usuario", "");
            // ... ponemos un tiempo de vida de 0 seg. para que se borre inmediatamente
            ck.setMaxAge(0);
            // la adjuntamos a la respuesta
            resp.addCookie(ck);
        }
    }
}
