package Services;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Arrays;
import java.util.Optional;

public class loginSvcImpl implements loginSvc{
    @Override
    public Optional<String> getUsuario(HttpServletRequest req) {
        // Leemos las cookies ...
        Cookie[] cks = req.getCookies() != null ? req.getCookies() : new Cookie[0];
        // ... Buscamos si existe la que hemos creado ...
        return Arrays.stream(cks)
                .filter(c -> "usuario".equals(c.getName()))
                .map(Cookie::getValue)
                .findFirst();
    }
}
